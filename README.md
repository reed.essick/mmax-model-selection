# mmax-model-selection

A small repository that houses scripts to perform model selection between NS and BH models for a component mass given a posterior distribution of Mmax from EOS constraints. 
A full description of available in [arXiv:2007.01372](https://arxiv.org/abs/2007.01372).

## Installation

This library can be installed in the standard way via

```
git clone https://git.ligo.org/reed.essick/mmax-model-selection
cd mmax-model-selection
python setup.py install
```

## Analyses

The main executable (`mmax-model-selection`) can be run with just a set of samples describing a single-event's posterior and the uncertainty in Mmax.
These are expected to be specified as uncompressed comma-separated value (CSV) ascii files.
The full help-string is reproduced below.

```
>> mmax-model-selection -h
usage: mmax-model-selection [-h]
                            [--event-max-num-samples EVENT_MAX_NUM_SAMPLES]
                            [--mass-column MASS_COLUMN]
                            [--spin-column SPIN_COLUMN]
                            [--m1-column M1_COLUMN] [--m2-column M2_COLUMN]
                            [--d-column D_COLUMN]
                            [--event-prior-samples EVENT_PRIOR_SAMPLES]
                            [--event-prior-max-num-samples EVENT_PRIOR_MAX_NUM_SAMPLES]
                            [--prior-mass-column PRIOR_MASS_COLUMN]
                            [--prior-spin-column PRIOR_SPIN_COLUMN]
                            [--prior-m1-column PRIOR_M1_COLUMN]
                            [--prior-m2-column PRIOR_M2_COLUMN]
                            [--prior-d-column PRIOR_D_COLUMN]
                            [--mmax-max-num-samples MMAX_MAX_NUM_SAMPLES]
                            [--mmax-column MMAX_COLUMN]
                            [--rmax-column RMAX_COLUMN]
                            [--mmax-weight-column MMAX_WEIGHT_COLUMN]
                            [--mmax-weight-is-log]
                            [--mmax-range MMAX_RANGE MMAX_RANGE]
                            [--q-range Q_RANGE Q_RANGE]
                            [--m-range M_RANGE M_RANGE]
                            [--mc-range MC_RANGE MC_RANGE]
                            [--d-range D_RANGE D_RANGE] [-p POPULATION]
                            [-P POPULATION_PARAMETER POPULATION_PARAMETER]
                            [--population-parameter-samples POPULATION_PARAMETER_SAMPLES]
                            [--population-parameter-max-num-samples POPULATION_PARAMETER_MAX_NUM_SAMPLES]
                            [--num-mc-normalization-samples NUM_MC_NORMALIZATION_SAMPLES]
                            [-v] [-V]
                            event_samples mmax_samples

a simple script to estimate bayes factors for a NS vs a BH based on m2
posteriors and Mmax limits

optional arguments:
  -h, --help            show this help message and exit

event posterior arguments:
  event_samples
  --event-max-num-samples EVENT_MAX_NUM_SAMPLES
  --mass-column MASS_COLUMN
                        values are expected to be in units of Msun
  --spin-column SPIN_COLUMN
                        if --spin-column is specified, we will interpret
                        --mmax-column as an estimate for Mtov and apply the
                        scaling of Breu+Rezzolla 2016 to estimate Mmax(Mtov,
                        spin, Rtov). Will also read in relevant data from
                        --spin-prior-column, --rmax-column
  --m1-column M1_COLUMN
                        only used when pruning posterior samples. Expected to
                        be in units of Msun
  --m2-column M2_COLUMN
                        only used when pruning posterior samples. Expected to
                        be in units of Msun
  --d-column D_COLUMN   only used when pruning posterior samples. Expected to
                        be in Mpc
  --event-prior-samples EVENT_PRIOR_SAMPLES
  --event-prior-max-num-samples EVENT_PRIOR_MAX_NUM_SAMPLES
  --prior-mass-column PRIOR_MASS_COLUMN
                        expected to be in units of Msun
  --prior-spin-column PRIOR_SPIN_COLUMN
  --prior-m1-column PRIOR_M1_COLUMN
                        only used when pruning posterior samples. Expected to
                        be in units of Msun
  --prior-m2-column PRIOR_M2_COLUMN
                        only used when pruning posterior samples. Expected to
                        be in units of Msun
  --prior-d-column PRIOR_D_COLUMN
                        only used when pruning posterior samples. Expected to
                        be in Mpc

Mmax posterior arguments:
  mmax_samples
  --mmax-max-num-samples MMAX_MAX_NUM_SAMPLES
  --mmax-column MMAX_COLUMN
                        values are expected to be in units of Msun
  --rmax-column RMAX_COLUMN
                        values are expected to be in km
  --mmax-weight-column MMAX_WEIGHT_COLUMN
  --mmax-weight-is-log
  --mmax-range MMAX_RANGE MMAX_RANGE

prior arguments:
  --q-range Q_RANGE Q_RANGE
  --m-range M_RANGE M_RANGE
                        expected to be in units of Msun
  --mc-range MC_RANGE MC_RANGE
                        expected to be in units of Msun
  --d-range D_RANGE D_RANGE
                        expected to be in Mpc
  -p POPULATION, --population POPULATION
                        must be one of: FLAT, PARETO_M2, PARETO_M1, LALINF,
                        PARETO, GAP_M2, GAP_M1, O3a_POWERLAW, GAP,
                        O3a_BROKEN_POWERLAW, O3a_POWERLAW+PEAK
  -P POPULATION_PARAMETER POPULATION_PARAMETER, --population-parameter POPULATION_PARAMETER POPULATION_PARAMETER
                        repeat this for each population parameter you want to
                        specify. e.g. "--population-param alpha 1.3"
  --population-parameter-samples POPULATION_PARAMETER_SAMPLES
                        if specified, will read in population parameters from
                        this CSV and average over the population weights from
                        each sample.Note, any values passed via --population-
                        parameters will overwrite anything read from this
                        file.
  --population-parameter-max-num-samples POPULATION_PARAMETER_MAX_NUM_SAMPLES
  --num-mc-normalization-samples NUM_MC_NORMALIZATION_SAMPLES
                        if --population-parameter-samples is specified, we
                        need to normalize the prior for each set of population
                        parameters in order to properly average over them.
                        There are some models that do not allow us to do this
                        numerically very conveniently, and so we normalize
                        with a direct monte-carlo estimate. That estimate is
                        done with this many samples (more samples will be more
                        accurate, but may take longer to run). DEFAULT=10000

verbosity arguments:
  -v, --verbose
  -V, --Verbose
```

The script estimates the posterior probability, the posterior odds ratio, and if prior samples are provied, a Bayes factor corresponding to whether the single-event's mass is below Mmax.
Values obtained from Monte-Carlo integration are reported as point-estimates with approximate standard deviations.
Note that we estiamte standard deviations without bootstrapping the data set; see [arXiv:2007.01372](https://arxiv.org/abs/2007.01372) for more details.
