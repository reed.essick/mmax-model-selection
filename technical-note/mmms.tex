\documentclass[aps, prd, twocolumn, preprintnumbers, superscriptaddress, nofootinbib, floatfix, linenumbers]{revtex4-2}

%-------------------------------------------------

\bibliographystyle{apsrev4-2}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{relsize}

\usepackage{graphicx}
\usepackage{color}

\usepackage[colorlinks=true,linkcolor=blue,citecolor=blue,urlcolor=blue]{hyperref}

%-------------------------------------------------

%\newcommand{\reed}[1]{\textcolor{blue}{#1}}
\newcommand{\reed}[1]{}

%\newcommand{\result}[1]{\textcolor{red}{#1}}
\newcommand{\result}[1]{#1}

%-------------------------------------------------

\begin{document}

%-------------------------------------------------

\title{On the consistency of an object's mass and spin with a neutron star accounting for uncertainty in the astrophysical population, equation of state, and measurement uncertainties for the individual event}

\author{Reed Essick}
\email{essick@cita.utoronto.ca}
\affiliation{Canadian Institute for Theoretical Astrophysics, 60 St. George St, Toronto, Ontario M5S 3H8}

\begin{abstract}
    I update~\citet{Essick:2020} to include marginalization over the uncertainty in the astrophysical population, including possible correlations with the neutron star equation of state, as well as conditioning on the maximum spin a neutron star can obtain (break-up spin) in order to provide estimators (and associated uncertainties) for the probability that an object's mass and spin are consistent with the physically allowed values for a neutron star.
    These probabilities are upper limits on the probability that an object actually is a neutron star.
\end{abstract}

\maketitle

%-------------------------------------------------

\section{Introduction}
\label{sec:introduction}

Upon the observation of a new astrophysical system, an immediate question that arises is what types of objects are contained within the system.
Gravitational wave (GW) observations with the advance LIGO~\cite{LIGO}, advanced Virgo~\cite{Virgo}, and KAGRA~\cite{KAGRA} interferometers can often constrain an object's mass, and sometimes its spin, but may not be able to confidently detect the presence of matter within the system.
This is particularly true for large masses (near the maximum neutron star (NS) mass), where the static tidal deformability becomes much smaller than the typical measurement uncertainties.
As such, it will not always be possible to determine whether an object is a NS or a BH by direct observation of the tidal deformability.
However, one can still place upper limits on the probability that an object is consistent with a NS.
This paper provides a formalism for computing such limits by assuming that every object that is consistent with the range of allowed masses and spins for a NS is indeed a NS.
I also marginalize over uncertainty in the NS equation of state (EoS), the total astrophysical distribution of events (e.g., masses and spins), and measurement uncertainty for a single event.

This work extends~\citet{Essick:2020}.
Similar analyses (with various simplifying assumptions) have been used in several LIGO-Virgo-KAGRA publications~\cite{GW190814, GW200105-GW200115, GWTC-3-RnP}.

This paper is structured as follows.
In Sec.~\ref{sec:estimators}, I write down expressions for the probability that an object is consistent with the allows masses and spins of a NS.
Sec.~\ref{sec:uncertainty} then provides expressions for Monte Carlo estimators and their uncertainty as a function of sample sets.
I conclude with discussion and demonstrations in Sec.~\ref{sec:discussion}.

%-------------------------------------------------

\section{Consistency with neutron stars}
\label{sec:estimators}

Define the range of single-event parameter space (mass and spin magnitude) that is consistent with a NS as
\begin{equation}
    \Theta_\mathrm{NS}(m, a|\varepsilon) = \Theta(m \leq m_\mathrm{max}(\varepsilon, a, m)) \Theta(a \leq a_\mathrm{max}(\varepsilon))
\end{equation}
where $m$ and $a$ are the object's mass and spin magnitude, respectively, and $\varepsilon$ is the EoS.
The functions $m_\mathrm{max}$ and $a_\mathrm{max}$ represent the maximum mass and spin that a NS can support.\footnote{Note that, depending on the EoS, there may also be a minimum mass for NSs. I ignore this, since the minimum mass allowed by the EoS ($\lesssim 0.5\,M_\odot$) is typically smaller than the minimum mass expected from astrophyiscal formation channels ($\sim 1\,M_\odot$).}
I take the universal relations reported in~\citet{Breu:2016}
\begin{align}
    a_\mathrm{max} & = (0.5543) C_\mathrm{TOV}^{-1/2} \\
    \frac{m_\mathrm{max}}{m_\mathrm{TOV}} & = 1 + (0.4283) C_\mathrm{TOV} a^2 + (0.7533) C_\mathrm{TOV}^2 a^4
\end{align}
where $C_\mathrm{TOV} = G m_\mathrm{TOV} / c^2 r_\mathrm{TOV}$ is the compactness of the star at the end of the stable sequence of solutions to the Tolman-Oppenheimer-Volkoff (TOV) equations~\cite{Tolman:1939, Oppenheimer:1939}: $m_\mathrm{TOV}$ is the maximum mass of a nonrotating NS and $r_\mathrm{TOV}$ is the corresponding radius.
Note that~\citet{Essick:2020} only considered information about the object's mass and did not include information about the object's spin.

I now wish to compute the probability that an object lives within this region.

I begin with a joint distribution over the population parameters ($\Lambda$), $\varepsilon$, and the single-event parameters ($\theta$, including $m$ and $a$) conditioned on some information ($\mathcal{H}$): $p(\Lambda, \varepsilon, \theta | \mathcal{H})$.
This can be an effective prior for the event or a posterior conditioned on a catalog that includes the event, etc.
I then compute
\begin{align}
    \mathcal{P}(\mathrm{NS}|\mathcal{H})
        & = \int d\Lambda d\varepsilon d\theta \, p(\Lambda, \varepsilon, \theta | \mathcal{H}) \Theta_\mathrm{NS}(\theta|\varepsilon) \nonumber \\
        & = \int d\Lambda d\varepsilon \, p(\Lambda, \varepsilon | \mathcal{H}) \int d\theta \, p(\theta | \Lambda, \mathcal{H}) \Theta_\mathrm{NS}(\theta | \varepsilon)
\end{align}
where, in the second line, I have separated the general distribution into two components: a joint distribution over the population and the EoS and a conditional distribution on the single-event parameters given the population.
This is convenient because one is often presented with samples drawn from a population hyperposterior\footnote{It is common to approximate the posteriors over $\Lambda$ and $\varepsilon$ as independent, but I do not make that assumption within this derivation.} $p(\Lambda,\varepsilon|\mathrm{data},\mathrm{model})$ and separate samples drawn from reference posteriors for an individual event $p(\theta|\mathrm{data}, \Lambda_\mathrm{ref})$.
These samples may also carry some weights (e.g., prior samples weighed by a likelihood represent a posterior).
It is then straightforward to approximate $\mathcal{P}$ via nested Monte Carlo sums.

Given a set of samples $\theta_k$ drawn from $p(\theta)$ and associated weights $w_k$ for each sample such that
\begin{equation}
    \int d\theta \, p(\theta|\Lambda_\mathrm{ref}, \mathcal{H}) F(\theta)
        \approx \frac{\sum\limits_k^M W_k F(\theta_k)}{\sum\limits_{k}^M W_k}
\end{equation}
One can add additional importance sampling weights to approximate
\begin{equation}
    \int d\theta \, p(\theta|\Lambda,\mathcal{H}) \Theta_\mathrm{NS}(\theta|\varepsilon) \approx
        \frac{\sum\limits_k^M w_k(\Lambda) \Theta_\mathrm{NS}(\theta_k|\varepsilon)}{\sum\limits_{k}^M w_k(\Lambda)}
\end{equation}
where
\begin{equation}
    w_k(\Lambda) = W_k \frac{p(\theta_k|\Lambda)}{p(\theta_k|\Lambda_\mathrm{ref})}
\end{equation}
Similarly, given samples ($\Lambda_p, \varepsilon_p$) drawn from $p(\Lambda, \varepsilon)$ and corresponding weights $\omega_p$ such that
\begin{equation}
    \int d\Lambda d\varepsilon \, p(\Lambda,\varepsilon|\mathcal{H}) F(\Lambda, \varepsilon) \approx
        \frac{\sum\limits_p^N \omega_p F(\Lambda_p, \varepsilon_p)}{\sum\limits_{p}^N \omega_p}
\end{equation}
one can approximate $\mathcal{P}$ with
\begin{equation}\label{eq:Phat}
    \hat{\mathcal{P}} = \frac{1}{\sum\limits_{p}^N \omega_p} \sum\limits_p^N \left[ \frac{\omega_p}{\sum\limits_{k}^M w_k(\Lambda_p)} \sum\limits_k^M \left[ w_k(\Lambda_p) \Theta_\mathrm{NS}(\theta_k|\varepsilon_p) \right] \right]
\end{equation}
Given $\hat{\mathcal{P}}$, one can additionally estimate the odds ratio
\begin{equation}
    \mathcal{O} = \frac{\mathcal{P}}{1 - \mathcal{P}}
\end{equation}
Given both posterior and prior odds ratios, one can estimate the Bayes factor
\begin{equation}
    \mathcal{B} = \frac{\mathcal{O}_\mathrm{post}}{\mathcal{O}_\mathrm{prior}}
\end{equation}
I now derive estimates for the uncertainty in such Monte Carlo estimates associated with the finite number of samples used.

%-------------------------------------------------

\section{Monte Carlo Uncertainty}
\label{sec:uncertainty}

I separate Eq.~\ref{eq:Phat} into four pieces and compute the moments of each in turn.
Specifically, I define
\begin{equation}
    \hat{\mathcal{P}} = \frac{F}{G}
\end{equation}
where
\begin{align}
    G & = \frac{1}{N} \sum\limits_p^N \omega_p \\
    F & = \frac{1}{N} \sum\limits_p^N \omega_p \frac{f(\Lambda_p, \varepsilon_p)}{g(\Lambda_p)}
\end{align}
and
\begin{align}
    g(\Lambda) & = \frac{1}{M} \sum\limits_k^M w_k(\Lambda) \\
    f(\Lambda, \varepsilon) & = \frac{1}{M} \sum\limits_k^M w_k(\Lambda) \Theta_\mathrm{NS}(\theta_k|\varepsilon)
\end{align}
I compute moments (expectation values $\mathrm{E}[\cdot]_x$, variances $\mathrm{V}[\cdot]_x$, and/or covariances $\mathrm{C}[\cdot,\cdot]_x$) of each under the measures associated with drawing samples $x$ (either $\theta_k \sim p(\theta)$ and $\Lambda_p, \varepsilon_p \sim p(\Lambda,\varepsilon)$).
I then approximate the moments of the ratio assuming the uncertainty in each sum is small
\begin{align}
    \mathrm{E}\left[\frac{a}{b}\right]_x & \approx \frac{\mathrm{E}[a]_x}{\mathrm{E}[b]_x} \label{eq:mean ratio} \\
    \mathrm{V}\left[\frac{a}{b}\right]_x & \approx \frac{1}{\mathrm{E}[b]_x^2} \mathrm{V}[a]_x + \frac{\mathrm{E}[a]_x^2}{\mathrm{E}[b]_x^4} \mathrm{V}[b]_x - \frac{2\mathrm{E}[a]_x}{\mathrm{E}[b]_x^3} \mathrm{C}[a,b]_x \label{eq:var ratio}
\end{align}
which comes from approximating errors as Gaussian and expanding the ratio in terms of small errors away from the expected values.

%------------------------

\subsection{Monte Carlo uncertainty from sums over single-event uncertainty}
\label{sec:single-event uncertainty}

Now, one can compute moments of Monte Carlo sums by integrating over the measure that defines how the samples were drawn
\begin{align}
    \mathrm{E}[g]_\theta & = \int \prod\limits_\kappa^M d\theta_\kappa \, p(\theta_\kappa) \left(\frac{1}{M} \sum_k^M w_k \right) \nonumber \\
        & = \int d\theta \, p(\theta) W(\theta) \frac{p(\theta|\Lambda)}{p(\theta|\Lambda_\mathrm{ref})} \nonumber \\
        & \propto \int d\theta \, p(\theta|\Lambda_\mathrm{ref}, \mathcal{H}) \frac{p(\theta|\Lambda)}{p(\theta|\Lambda_\mathrm{ref})} \nonumber \\
        & \propto \frac{p(\mathcal{H}|\Lambda)}{p(\mathcal{H}|\Lambda_\mathrm{ref})}
\end{align}
Similarly, by computing the second moment of $g$, I obtain
\begin{multline}
    \mathrm{V}[g]_\theta = \frac{1}{M} \left( \int d\theta\, p(\theta) w(\theta,\Lambda)^2 \right. \\
        \left. - \left(\int d\theta\, p(\theta) w(\theta,\Lambda) \right)^2 \right)
\end{multline}
I can approximate both these moments with sums over the samples
\begin{align}
    \mathrm{E}[g]_\theta & \approx \frac{1}{M} \sum\limits_k^M w_k(\Lambda) \\
    \mathrm{V}[g]_\theta & \approx \frac{1}{M} \left( \frac{1}{M}\sum_k^M w_k^2 - \mathrm{E}[g]_\theta^2 \right)
\end{align}
Similarly, I obtain estimates for the moment of $f$
\begin{align}
    \mathrm{E}[f]_\theta & \approx \frac{1}{M} \sum\limits_k^M w_k \Theta_k \\
    \mathrm{V}[f]_\theta & \approx \frac{1}{M} \left( \frac{1}{M} \sum\limits_k^M w_k^2 \Theta_k - \mathrm{E}[f]_\theta^2 \right)
\end{align}
where $\Theta_k = \Theta_\mathrm{NS}(\theta_k|\varepsilon)$ and
\begin{equation}
    \mathrm{C}[f,g]_\theta \approx \frac{1}{M} \left( \frac{1}{M}\sum\limits_k^M w_k^2 \Theta_k - \mathrm{E}[f]_\theta \mathrm{E}[g]_\theta \right)
\end{equation}

Noting that these moments depend on both $\Lambda$ and $\varepsilon$, one can approximate $\mathrm{E}[f/g]_\theta$ and $\mathrm{V}[f/g]_\theta$ for each sample $(\Lambda_p, \varepsilon_p)$ separately via Eqs.~\ref{eq:mean ratio} and~\ref{eq:var ratio}.

%------------------------

\subsection{Monte Carlo uncertainty from sums over population uncertainty}
\label{sec:pop uncertainty}

I now consider the uncertainty from the finite number of population samples.
Following a similar procedure as Sec.~\ref{sec:single-event uncertainty}, it is straightforward to show that
\begin{align}
    \mathrm{E}[G]_{\Lambda} & \approx \frac{1}{N} \sum\limits_p^N \omega_p \\
    \mathrm{V}[G]_{\Lambda} & \approx \frac{1}{N} \left( \frac{1}{N}\sum\limits_p^N \omega_p^2 - \mathrm{E}[G]_{\Lambda}^2 \right)
\end{align}
The moments of $F$ are slightly more complicated, but it is also possible to show that
\begin{equation}
    \mathrm{E}[F]_{\Lambda,\varepsilon,\theta} \approx \frac{1}{N} \sum\limits_p^N \omega_p \mathrm{E}\left[\frac{f_p}{g_p}\right]_\theta
\end{equation}
where $f_p = f(\Lambda_p, \varepsilon_p)$ and $g_p = g(\Lambda_p)$.
One can also show that
\begin{align}
    \mathrm{E}[F^2]_{\Lambda,\varepsilon,\theta}
        & \approx \frac{1}{N} \left( \frac{1}{N} \sum\limits_p^N \omega_p^2 \mathrm{E}\left[\left(\frac{f_p}{g_p}\right)^2\right]_\theta \right) \nonumber \\
        & \quad\quad + \left(1 - \frac{1}{N}\right) \frac{1}{N^2} \sum\limits_p^N \omega_p \sum\limits_q^N \omega_q \mathrm{E}\left[\frac{f_p}{g_p}\frac{f_q}{g_q}\right]_\theta
\end{align}
expanding the moments with respect to $\theta$ in terms of expected values and (co)variances yields
\begin{align}
    \mathrm{V}[F]_{\Lambda,\varepsilon,\theta}
        & \approx \frac{1}{N} \left( \frac{1}{N} \sum\limits_p^N \omega_p^2 \mathrm{E}\left[\frac{f_p}{g_p}\right]_\theta^2 - \mathrm{E}[F]_{\Lambda,\varepsilon,\theta}^2 \right. \nonumber \\
        & \left. \quad \quad \quad \quad + \left(2 - \frac{1}{N}\right) \frac{1}{N} \sum\limits_p^N \omega_p^2 \mathrm{V}\left[\frac{f_p}{g_p}\right]_\theta \right) \nonumber \\
        & \quad + \left(1 - \frac{1}{N}\right) \frac{1}{N^2} \sum\limits_{p}^N \omega_p \sum\limits_{q\neq p}^N \omega_q \mathrm{C}\left[\frac{f_p}{g_p}\frac{f_q}{g_q}\right]_\theta \label{eq:var F}
\end{align}

Let us attempt to interpret each of these terms in turn.
The first term is the standard expression for the uncertainty in a Monte Carlo sum assuming the estimate $\mathrm{E}[f/g]_\theta$ is known exactly for each ($\Lambda, \varepsilon$) sample.
The second and third lines account for the additional uncertainty from the fact that $\mathrm{E}[f/g]_\theta$ is not a perfect estimator.
If one considers the second line by itself, this is proportional to the variance expected from adding $N$ uncorrelated noisy estimators together, each with a weight $\omega_p/N$.
This term will vanish as $O(1/N)$ as the number of population samples increases.
This is because, even if each individual estimator is noisy, the sum of a very large number of noisy estimators can still be a precise estimator.
The third line, however, does not (in general) vanish as $N\rightarrow\infty$.
This represents the residual uncertainty from the fact that I reuse the same single-event parameter samples for each population sample.

One could estimate $\mathrm{C}[(f_p/g_p)(f_q/g_q)]_\theta$ directly for all pairs of population samples, but this scales as $N(N+1)/2$ and may be extremely costly for large sample sizes.
Instead, I consider the upper limit
\begin{equation}
    \mathrm{C}\left[\left(\frac{f_p}{g_p}\right)\left(\frac{f_q}{g_q}\right)\right]_\theta \leq \sqrt{\mathrm{V}\left[\frac{f_p}{g_p}\right]_\theta \mathrm{V}\left[\frac{f_q}{g_q}\right]_\theta}
\end{equation}
and obtain
\begin{align}
    \mathrm{V}&[F]_{\Lambda,\varepsilon,\theta} \lesssim \nonumber \\
        & \frac{1}{N} \left( \frac{1}{N} \sum\limits_p^N \omega_p^2 \mathrm{E}\left[\frac{f_p}{g_p}\right]_\theta^2 - \left(\frac{1}{N}\sum\limits_p^N \omega_p \mathrm{E}\left[\frac{f_p}{g_p}\right]_\theta\right)^2 \right) \nonumber \\
        & \quad + \frac{1}{N}\left( \frac{1}{N} \sum\limits_p^N \omega_p^2 \mathrm{V}\left[\frac{f_p}{g_p}\right]_\theta - \left(\frac{1}{N} \sum\limits_p^N \omega_p \mathrm{V}\left[\frac{f_p}{g_p}\right]_\theta^{1/2}\right)^2 \right) \nonumber \\
        & \quad\quad + \left( \frac{1}{N} \sum\limits_p^N \omega_p \mathrm{V}\left[\frac{f_p}{g_p}\right]_\theta^{1/2} \right)^2
\end{align}
Again, the first two lines vanish as $O(1/N)$, but the third line remains finite.
This persistent term is an averaged uncertainty from the individual estimates derived from sums over single-event parameter uncertainty, and corresponds to the variance in the sum of many perfectly correlated variates.

Finally, I can also approximate
\begin{equation}
    \mathrm{C}[F,G]_{\Lambda,\varepsilon,\theta} \approx \frac{1}{N} \left( \frac{1}{N}\sum\limits_p^N \omega_p^2 \mathrm{E}\left[\frac{f_p}{g_p}\right]_\theta - \mathrm{E}[G]_\Lambda \mathrm{E}[F]_{\Lambda,\varepsilon,\theta} \right)
\end{equation}

With these expressions, we can then approximate $\hat{\mathcal{P}}$ and its uncertainty via Eqs.~\ref{eq:mean ratio} and~\ref{eq:var ratio}.

%------------------------

\subsection{Additional moments needed for Bayes factors}
\label{sec:bayes moments}

To compute a Bayes factor, I use the posterior and prior odds.
Defining $F$ as above using the single-event reference posterior and an analogous quantity ($H$) that instead uses the single-event reference prior, I estimate
\begin{equation}
    \mathcal{B} \approx \frac{F/(G-F)}{H/(G-H)}
\end{equation}
To estimate the uncertainty in $\mathcal{B}$, I additionally need
\begin{multline}
    \mathrm{C}[F,H]_{\Lambda,\varepsilon,\mathrm{prior},\mathrm{post}} \approx \\
        \frac{1}{N} \left( \frac{1}{N}\sum\limits_p^N \omega_p^2 \mathrm{E}\left[\frac{f_p}{g_p}\right]_\mathrm{prior} \mathrm{E}\left[\frac{f_p}{g_p}\right]_\mathrm{post} \right. \\
        - \mathrm{E}[H]_{\Lambda,\varepsilon,\mathrm{prior}} \mathrm{E}[F]_{\Lambda,\varepsilon,\mathrm{post}} \mathlarger{\mathlarger{\mathlarger{)}}}
\end{multline}
I can then estimate $\mathrm{V}[\mathcal{B}]_{\Lambda,\varepsilon,\mathrm{prior},\mathrm{post}}$ based on the expected values and (co)variances for $F$, $H$, and $G$.
\begin{equation}
    \mathrm{V}[\mathcal{B}]_{\Lambda,\varepsilon,\mathrm{prior},\mathrm{post}} \approx \sum\limits_{ij} d\mathcal{B}_i \mathcal{C}_{ij} d\mathcal{B}_j
\end{equation}
where
\begin{equation}
    d\mathcal{B} = \begin{bmatrix} d\mathcal{B}/dF & d\mathcal{B}/dH & d\mathcal{B}/dG \end{bmatrix}^\mathrm{T}
\end{equation}
and
\begin{equation}
    \mathcal{C} =
    \begin{bmatrix}
        \mathrm{V}[F]_{\Lambda,\varepsilon,\mathrm{post}} & \mathrm{C}[F,H]_{\Lambda,\varepsilon,\mathrm{prior},\mathrm{post}} & \mathrm{C}[F,G]_{\Lambda,\varepsilon,\mathrm{post}} \\
        \mathrm{C}[F,H]_{\Lambda,\varepsilon,\mathrm{prior},\mathrm{post}} & \mathrm{V}[H]_{\Lambda,\varepsilon,\mathrm{prior}} & \mathrm{C}[H,G]_{\Lambda,\varepsilon,\mathrm{prior}} \\
        \mathrm{C}[F,G]_{\Lambda,\varepsilon,\mathrm{post}} & \mathrm{C}[H,G]_{\Lambda,\varepsilon,\mathrm{prior}} & \mathrm{V}[G]_{\Lambda} \\
    \end{bmatrix}
\end{equation}

%-------------------------------------------------

\section{Discussion and demonstrations}
\label{sec:discussion}

Sec.~\ref{sec:uncertainty} shows that the overall uncertainty in $\hat{\mathcal{P}}$ from the finite number of samples provided depends on both the number of population sapmles provided ($N$) and the number of single-event posterior samples provided ($M$).
The total error will not vanish unless both $N$ and $M$ diverge.
However, the computational cost scales as $O(NM)$ for both the expected value and our upper limit on the variance.\footnote{The cost would scale as $O(N^2 M)$ if we computed the full covariance matrix $\mathrm{C}[f_pf_q/g_pg_q]_\theta$.}
In general, then, we must limit ourselves to a reasonable number of each type of sample in order to limit the cost of the computation.

The expressions in Sec.~\ref{sec:uncertainty} are implemented within the publicly available library \texttt{mmax-model-selection}~\cite{mmax-model-selection}, which in turn depends on \texttt{gw-distributions}~\cite{gw-distributions} for models of the astrophysical population.
These libraries are lightweight and, \result{for $N \sim M \sim 1000$, estimates of the prior/posterior probabilities, odds ratios, and bayes factor can be run on a laptop (a single i7-1185G7 CPU clocked at 3 GHz) in $O(15)$ sec}.

\reed{
Show figures demonstrating the impact of population uncertainty
\begin{itemize}
    \item show distributions in $m/m_\mathrm{max}$ vs. $a/a_\mathrm{max}$ assuming a few fixed populations
        \begin{itemize}
            \item a reference population
            \item two extremes of the same population model (as supported by, e.g., a fit to O3)
            \item two different population models (Amanda's fits with and without the break-point in the pairing function? re-do these fits for myself? should be fast with numpyro implementation...)
        \end{itemize}
    \item also show it marginalized over the population uncertainty.
\end{itemize}
}

%-------------------------------------------------

\bibliography{refs}

%-------------------------------------------------

\end{document}

%-------------------------------------------------
