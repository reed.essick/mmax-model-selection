"""logic associated with fits from Breu+Rezzolla 2016 (https://academic.oup.com/mnras/article/459/1/646/2608837)
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import numpy as np

### non-standard libraries
from .units import (CGS_G, CGS_Msun, CGS_c)

#-------------------------------------------------

def compactness(Mtov, Rtov):
    '''compute the compactness with the correct dimensionful quantities. Assumes Mtov is in [Msun] and Rtov is in [km]
    '''
    return (CGS_G * CGS_Msun * Mtov) / (CGS_c**2 * Rtov * 1e5)

#-------------------------------------------------

BR2016_b4 = 5.543e-1

def br2016_max_spin(Mtov, Rtov):
    '''estimate the maximum spin based on Eqn 17 of Breu+Rezzolla 2016 (https://academic.oup.com/mnras/article/459/1/646/2608837)
    such that
        max_spin \propto (Rtov/Mtov)**0.5
    '''
    return BR2016_b4 * compactness(Mtov, Rtov)**-0.5

#------------------------

BR2016_c2 = 4.283e-1
BR2016_c4 = 7.533e-1

def br2016_mmax_scaling(spin, Mtov, Rtov):
    '''modify the mass of the object based on it's spin so that we can make a fair comparison against Mmax(Mtov, spin)
    Specifically, we assume the universal relation from Eqn 18 of Breu+Rezzolla 2016 (https://academic.oup.com/mnras/article/459/1/646/2608837)
    such that
        Mmax = Mtov * br2016_scaling(spin, Mtov, Rtov)
    we return Mmax from this scaling
    '''
    # the compactness in natural units, assuming Mtov is in units of Msun and Rtov is in km
    Ctov = compactness(Mtov, Rtov)
    return Mtov * (1 + BR2016_c2*Ctov*spin**2 + BR2016_c4*(Ctov**2)*(spin**4))
