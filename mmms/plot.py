"""plotting logic
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os

import numpy as np

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
plt.rcParams['font.family'] = 'serif'
plt.rcParams['text.usetex'] = True

### non-stardard libraries
from .engine import (average_importance_sampling_weight, importance_sampling_weight)
from .utils import bounds
from .br2016 import (br2016_mmax_scaling, br2016_max_spin)

#-------------------------------------------------

MAX_BINS = 500
MIN_BINS = 10

TICK_PARAMS = {
    'left' : True,
    'right' : True,
    'top' : True,
    'bottom' : True,
    'direction' : 'in',
    'which' : 'both',
}

#-------------------------------------------------

def hist_axes(fig):
    """generate axes for 1D histogram
    """
    return fig.gca()
    
#------------------------

def hist(
        ax,
        samples,
        weights=None,
        label=None,
        color='k',
        alpha=1.0,
        linestyle='solid',
        orientation='vertical',
        binscale=0.5,
    ):
    """make a 1D histogram of weighed samples
    """
    if weights is None:
        weights = np.ones(len(samples), dtype=float)/len(samples)

    samples = samples[weights>0]
    weights = weights[weights>0]

#    lim, (m, M) = bounds(samples, weights)
    lim, _ = bounds(samples, weights)
    m, M = lim

    bins = np.linspace(m, M, min(MAX_BINS, max(MIN_BINS, int(binscale*len(samples)**0.5))))

    weights = weights[:] / np.sum(weights) ### make sure they're normalized
    weights /= bins[1]-bins[0]             ### normalize the scale height

    ans = ax.hist(
        samples,
        bins=bins,
        weights=weights,
        label=label,
        color=color,
        alpha=alpha,
        linestyle=linestyle,
        histtype='step',
        orientation=orientation,
    )
    if orientation == 'horizontal':
        ax.set_ylim(lim)
    elif orientation == 'vertical':
        ax.set_xlim(lim)
    else:
        raise ValueError('orientation=%s not understood!'%orientation)

    ax.tick_params(**TICK_PARAMS)

    return ans, lim

#-------------------------------------------------

def hist2d_axes(fig):
    """generate axes for a 2D histogram
    """
    low, mid, high = 0.1, 0.75, 0.95

    ax = fig.add_axes([low, low, mid-low, mid-low])
    axm = fig.add_axes([low, mid, mid-low, high-mid]) 
    axs = fig.add_axes([mid, low, high-mid, mid-low]) 

    for a in [ax, axm, axs]:
        a.tick_params(**TICK_PARAMS)

    return ax, axm, axs

#------------------------

def hist2d(ax, x, y, weights=None, colormap='Blues', binscale=0.25):
    """make a 2D histogram of weighed samples
    """
    N = len(x)
    assert N==len(y), 'x and y  must be the same length'

    if weights is None:
        weights = np.ones(len(x), dtype=float)/len(x)
    else:
        assert N==len(weights), 'weights must be the same length as x and y'

    xlim, (xm, xM) = bounds(x, weights)
    ylim, (ym, yM) = bounds(y, weights)

    nbins = min(MAX_BINS, max(MIN_BINS, int(binscale*N**0.5)))
    xbins = np.linspace(xm, xM, nbins)
    ybins = np.linspace(ym, yM, nbins)

    weights = weights[:] / np.sum(weights)
    weights /= (xbins[1]-xbins[0])*(ybins[1]-ybins[0])

    ans = ax.hist2d(
        x,
        y,
        bins=[xbins, ybins],
        weights=weights,
        cmap=colormap,
    )
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)

    ### FIXME: add a colorbar?

    return ans, xlim, ylim

#-------------------------------------------------

def hist_mass_distribution(
        samples,
        mass,
        mass_priors,
        mass_weights,
        population,
        pop_samples,
        mtov,
        pop_weights,
    ):

    fig = plt.figure()
    ax = hist_axes(fig)

    # plot the Mmax distribution
    (n, b, _), (m, M) = hist(
        ax,
        mtov,
        weights=pop_weights,
        label='$p(M_\mathrm{TOV})$',
        color='k',
        alpha=1.0,
        linestyle='solid',
        binscale=0.25,
    )

    # plot original mass distribution
    (n, b, _), lim = hist(
        ax,
        mass,
        weights=mass_weights,
        label='original $p(m)$',
        color='b',
        alpha=0.5,
        linestyle='dashed',
        binscale=0.5,
    )
    m = min(lim[0], m)
    M = max(lim[1], M)

    # plot population-weighed mass distribution
    (n, b, _), lim = hist(
        ax,
        mass,
        weights=average_importance_sampling_weight(
            population,
            pop_samples,
            pop_weights,
            samples,
            mass_priors,
            mass_weights,
        ),
        label='reweighted $p(m)$',
        color='b',
        alpha=1.0,
        linestyle='solid',
        binscale=0.5,
    )

    m = min(lim[0], m)
    M = max(lim[1], M)

    ### decorate
    ax.set_xlabel('$m\, [M_\odot]$')
    ax.set_xlim(xmin=m, xmax=M)

    plt.setp(ax.get_yticklabels(), visible=False)

    ax.legend(loc='best')

    # return
    return fig

#-------------------------------------------------

def hist_mass_spin_distribution(
        samples,
        mass,
        spin,
        priors,
        weights,
        population,
        pop_samples,
        mtov,
        rtov,
        pop_weights,
        verbose=False,
    ):

    ### compute the distribution via monte-carlo pairings
    if verbose:
        print('computing points for all possible pairs of mass and pop samples')
    mass_ratio = []
    spin_ratio = []
    sample_weights = []

    # iterate over population so we can compute importance sampling weights
    for M, R, POP, W in zip(mtov, rtov, pop_samples, pop_weights):
        mass_ratio.append(mass/br2016_mmax_scaling(spin, M, R))
        spin_ratio.append(spin/br2016_max_spin(M, R))
        sample_weights.append(importance_sampling_weight(population, POP, samples, priors, weights) * W)

    mass_ratio = np.concatenate(tuple(mass_ratio))
    spin_ratio = np.concatenate(tuple(spin_ratio))
    sample_weights = np.concatenate(tuple(sample_weights))

    #---

    ### now plot a basic histogram
    fig = plt.figure()
    ax, axm, axs = hist2d_axes(fig)
    binscale = 0.05

    # plot projected histogram of mass ratios
    hist(
        axm,
        mass_ratio,
        weights=sample_weights,
        color='b',
        alpha=1.0,
        linestyle='solid',
        orientation='vertical',
        binscale=binscale,
    )

    # plot projected histogram of spin ratios
    hist(
        axs,
        spin_ratio,
        weights=sample_weights,
        color='b',
        alpha=1.0,
        linestyle='solid',
        orientation='horizontal',
        binscale=binscale,
    )

    # plot the joint distribution
    _, (xmin, xmax), (ymin, ymax) = hist2d(
        ax,
        mass_ratio,
        spin_ratio,
        weights=sample_weights,
        colormap='Blues',
        binscale=binscale,
    )

    # draw shading and reference lines
    if (xmin < 1) and (1 < xmax) and (ymin < 1) and (1 < ymax):
        ax.fill_between(
            [xmin, 1.0, 1.0, xmax],
            [1, 1, 0, 0],
            [ymax, ymax, ymax, ymax],
            color='k',
            alpha=0.25,
        )

    elif (xmin < 1) and (1 < xmax):
        ax.fill_between([1, xmax], [ymin]*2, [ymax]*2, color='k', alpha=0.25)

    elif (ymin < 1) and (1 < ymax):
        ax.fill_between([xmin, xmax], [1, 1], [ymax]*2, color='k', alpha=0.25)

    if (xmin < 1) and (1 < xmax):
        ylim = axm.get_ylim()
        axm.plot([1]*2, ylim, color='k')
        axm.set_ylim(ylim)

    if (ymin < 1) and (1 < ymax):
        xlim = axs.get_xlim()
        axs.plot(xlim, [1.0]*2, color='k')
        axs.set_xlim(xlim)

    ax.set_xlim(xmin=xmin, xmax=xmax)
    ax.set_ylim(ymin=ymin, ymax=ymax)

    axm.set_xlim(ax.get_xlim())
    axs.set_ylim(ax.get_ylim())

    ### finish decorating
    ax.set_xlabel('$m/M_\mathrm{max}(M_\mathrm{TOV}, R_\mathrm{TOV}, |a|)$')
    ax.set_ylabel('$|a|/a_\mathrm{max}(M_\mathrm{TOV}/R_\mathrm{TOV})$')

    for _ in [axm, axs]:
        plt.setp(_.get_xticklabels(), visible=False)
        plt.setp(_.get_yticklabels(), visible=False)

    # return
    return fig

#-------------------------------------------------

def save(fig, base, figtypes=['png'], dpi=200, verbose=False):
    for figtype in figtypes:
        figname = base % figtype
        if verbose:
            print('saving: '+figname)
        fig.savefig(figname, dpi=dpi)

def close(*args, **kwargs):
    plt.close(*args, **kwargs)
