"""a library with Monte Carlo summation for the model selection problem
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import numpy as np

### non-standard libraries
from .br2016 import (br2016_mmax_scaling, br2016_max_spin)

#-------------------------------------------------
#
# Basic sanity checks associated with variances
#
#-------------------------------------------------

def _check_variance(variance):
    assert (variance > 0) or np.isclose(variance, 0), \
        'bad estimate for the variance (%.6e)' % variance
    return max(0, variance)

#-------------------------------------------------
#
# Logic associated with importance sampling (reweighing single-event posterior samples)
#
#-------------------------------------------------

def average_importance_sampling_weight(population, pop_samples, pop_weights, samples, priors, weights):
    """compute the averaged importance sampling weight for each sample. Average is performed over pop_samples
    """
    ave_weights = np.zeros(len(samples), dtype=float)
    for weight, samp in zip(pop_weights, pop_samples):
        ave_weights += weight * importance_sampling_weight(population, samp, samples, priors, weights)

    return ave_weights / np.sum(ave_weights)

#-----------

def importance_sampling_weight(population, pop_sample, samples, priors, weights):
    """compute the importance sampling weights for each sample
    """
    # update the population model
    for key, val in zip(pop_sample.dtype.names, pop_sample):
        population[key] = val

    # compute importance sampling weights, including any "pre-existing" weights
    weights = weights * population.prob(samples) / priors

    # return normalized weights (will automatically account for whether pop is a good fit to the data)
    return weights / np.sum(weights)

#-------------------------------------------------
#
# logic associated with Monte Carlo sums over population uncertainty with fixed single-event parameters
#
#-------------------------------------------------

def weigh_mass_samples(mass, mtov, spin=None, rtov=None, pop_weights=None):
    """compute the probability that mass<=mmax for each mass sample, returning an array of weights.
NOTE! this does *not* reweigh mass samples according to the population.
    """
    if mmax_weights is None:
        mmax_weights = np.ones(len(mmax), dtype=float)
    if (spin is not None) and (rtov is not None):
        return np.array([np.sum(pop_weights \
            * (M <= br2016_mmax_scaling(S, mtov, rtov)) \
            * (np.abs(S) <= br2016_max_spin(mtov, rtov))) \
            for M, S in zip(mass, spin)], dtype=float)
    else:
        return np.array([np.sum(pop_weights * (M <= mtov)) for M in mass], dtype=float)

#-------------------------------------------------
#
# logic associated with Monte Carlo sums over both population and single-event parameter uncertainty
#
#-------------------------------------------------

def _compute_mass_moments(
        population,
        pop_sample,
        mtov,
        samples,
        mass,
        priors,
        weights,
        spin=None,
        rtov=None,
        verbose=False,
    ):
    """compute moments over the single-event parameter uncertainty for fixed population parameters
    """
    # compute the weights needed within the sums over single-event uncertainty
    if verbose:
        print('computing importance sampling weights for sums over single-event uncertainty')
    mass_weights = importance_sampling_weight(population, pop_sample, samples, priors, weights)
    M = len(mass_weights)

    # compute the condition to be evaluated within the sums over single-event uncertainty
    if verbose:
        print('computing conditional')
    if (spin is not None) and (rtov is not None):
        condition = (mass <= br2016_mmax_scaling(spin, mtov, rtov)) * (np.abs(spin) <= br2016_max_spin(mtov, rtov))
    else:
        condition = mass <= mtov

    # perform the sums
    if verbose:
        print('computing sums over single-event uncertainty')
    sum_weights = np.sum(mass_weights) / M
    sum_weights2 = np.sum(mass_weights**2) / M

    sum_weights_condition = np.sum(mass_weights * condition) / M
    sum_weights2_condition = np.sum(mass_weights**2 * condition) / M

    # combine the to estimate moments
    if verbose:
        print('computing moments over single-event uncertainty')
    f_exp = sum_weights_condition
    f_var = (sum_weights2_condition - sum_weights_condition**2) / M

    g_exp = sum_weights
    g_var = (sum_weights2 - sum_weights**2) / M

    fg_cov = (sum_weights2_condition - sum_weights*sum_weights_condition) / M

    # combine to estimate moments of the ratio
    ratio_exp = f_exp / g_exp
    ratio_var = (f_var / g_exp**2) + (f_exp**2 * g_var / g_exp**4) - (2 * f_exp * fg_cov / g_exp**3)

    # make sure we have a physical estimate for the variance
    ratio_var = _check_variance(ratio_var)

    # return
    return ratio_exp, ratio_var

#-----------

def _compute_vec_mass_moments(
        population,
        pop_samples,
        mtov,
        samples,
        mass,
        priors,
        weights,
        spin=None,
        rtov=None,
        verbose=False,
    ):
    """compute the mass moments for every population sample specified
    """
    N = len(pop_samples)
    mass_moments = np.empty((N,2), dtype=float)
    for ind in range(N):
        mass_moments[ind] = _compute_mass_moments(
            population,
            pop_samples[ind],
            mtov[ind],
            samples,
            mass,
            priors,
            weights,
            spin=spin,
            rtov=None if rtov is None else rtov[ind],
            verbose=verbose,
        )
    return mass_moments

#------------------------

def _vec_mass_moments2pop_moments(pop_weights, mass_moments, verbose=False):
    """compute pop moments from vectors of mass moments
    """
    N = len(pop_weights)

    mass_m1 = mass_moments[:,0]
    mass_var = mass_moments[:,1]

    mass_m2 = mass_var + mass_m1**2
    mass_stdv = mass_var**0.5

    # compute sums over population uncertainty
    if verbose:
        print('computing sums over population uncertainty')

    sum_weights = np.sum(pop_weights) / N
    sum_weights2 = np.sum(pop_weights**2) / N

    sum_weights_m1 = np.sum(pop_weights * mass_m1) / N
    sum_weights2_m1 = np.sum(pop_weights**2 * mass_m1) / N

    sum_weights2_m12 = np.sum(pop_weights**2 * mass_m1**2) / N
    sum_weights2_var = np.sum(pop_weights**2 * mass_var) / N

    sum_weights_stdv = np.sum(pop_weights * mass_stdv) / N

    # combine sums into moments
    if verbose:
        print('computing moments over population uncertainty')

    G_exp = sum_weights
    G_var = (sum_weights2 - sum_weights**2) / N
    G_var = _check_variance(G_var)

    F_exp = sum_weights_m1
    # the following is an upper limit on the variance of F
    F_var = ((sum_weights2_m12 - sum_weights_m1**2) + sum_weights2_var) / N + (1 - 1/N)*sum_weights_stdv**2
    F_var = _check_variance(F_var)

    FG_cov = (sum_weights2_m1 - sum_weights*sum_weights_m1) / N

    # return
    return (F_exp, F_var), (G_exp, G_var), FG_cov

#-----------

def _compute_pop_moments(
        population,
        pop_samples,
        mtov,
        pop_weights,
        samples,
        mass,
        priors,
        weights,
        spin=None,
        rtov=None,
        verbose=False,
        Verbose=False,
    ):
    """compute moments over the population uncertainty by computing moments over single-event parameter \
uncertainty (at each pop_sample)
    """
    verbose |= Verbose

    ### compute mass moments for each population sample
    if verbose:
        print('computing moments over single-event uncertainty for each population sample separately')

    mass_moments = _compute_vec_mass_moments(
        population,
        pop_samples,
        mtov,
        samples,
        mass,
        priors,
        weights,
        spin=spin,
        rtov=rtov,
        verbose=Verbose,
    )

    return _vec_mass_moments2pop_moments(pop_weights, mass_moments, verbose=verbose)

#-----------

def _compute_all_pop_moments(
        population,
        pop_samples,
        mtov,
        pop_weights,
        post_samples,
        post_mass,
        post_priors,
        post_weights,
        prior_samples,
        prior_mass,
        prior_priors,
        prior_weights,
        post_spin=None,
        prior_spin=None,
        rtov=None,
        verbose=False,
        Verbose=False,
    ):
    """compute all moments over population uncertainty for both prior and posterior single-event uncertainty
    """
    verbose |= Verbose

    ### compute mass moments for each population sample
    if verbose:
        print('computing moments over single-event posterior uncertainty for each population sample separately')

    post_moments = _compute_vec_mass_moments(
        population,
        pop_samples,
        mtov,
        post_samples,
        post_mass,
        post_priors,
        post_weights,
        spin=post_spin,
        rtov=rtov,
        verbose=Verbose,
    )

    if verbose:
        print('computing moments over single-event prior uncertainty for each population sample separately')

    post_moments = _compute_vec_mass_moments(
        population,
        pop_samples,
        mtov,
        prior_samples,
        prior_mass,
        prior_priors,
        prior_weights,
        spin=prior_spin,
        rtov=rtov,
        verbose=Verbose,
    )

    ### now compute moments

    # posterior
    (F, F_var), (G, G_var), FG_cov = _vec_mass_moments2pop_moments(
        pop_weights,
        post_moments,
        verbose=verbose,
    )

    # prior
    (H, H_var), (G, G_var), HG_cov = _vec_mass_moments2pop_moments(
        pop_weights,
        prior_moments,
        verbose=verbose,
    )

    # covariance between posterior and prior
    N = len(pop_weights)
    FH_cov = (p.sum(pop_weights * post_moments[:,0] * prior_moments[:,0]) / N - F*H) / N

    ### return
    return (F, F_var), (H, H_var), (G, G_var), FG_cov, HG_cov, FH_cov

#-------------------------------------------------
#
# high-level wrapper functions that take MC moments and compute probabilties
#
#-------------------------------------------------

def _moments2prob(exp_f, exp_g, var_f, var_g, cov_fg):
    exp_P = exp_f/exp_g
    var_P = var_f/exp_g**2 + var_g*exp_f**2/exp_g**4 - 2*cov_fg*exp_f/exp_g**3

    var_P = _check_variance(var_P) # sanity check

    return exp_P, var_P**0.5

#-----------

def _moments2odds(exp_f, exp_g, var_f, var_g, cov_fg):
    exp_O = exp_f / (exp_g - exp_f)
    exp_d = exp_g - exp_f ### a useful thing to have
    var_O = var_f*exp_g**2/exp_d**4 + var_g*exp_f**2/exp_d**4 - 2*cov_fg*exp_f*exp_g/exp_d**4

    var_O = _check_variance(var_O) # sanity check

    return exp_O, var_O**0.5

#-----------

def _moments2bayes(exp_f, exp_g, exp_h, var_f, var_g, var_h, cov_fg, cov_fh, cov_hg):

    # expected value
    B = (F * (G-H)) / (H * (G-F))

    # compute jacobians
    dBdF = B/F + B/(G-F)
    dBdH = - B/H - B/(G-H)
    dBdG = B/(G-H) - B/(G-F)

    vec = np.array([dBdF, dBdH, dBdG], dtype=float)

    # set up covariance matrix

    cov = np.array(
        [[var_f,  cov_fh, cov_fg],
         [cov_fh, var_h,  cov_hg],
         [cov_fg, cov_hg, var_g]],
        dtype=float,
    )

    # compute variance as an inner product
    B_var = np.dot(np.dot(cov, vec), vec)

    B_var = _check_variance(B_var) # sanity check

    # return
    return B, B_var**0.5

#-------------------------------------------------
#
# highest-level wrapper functions that take samples and compute probabilties
#
#-------------------------------------------------

def samples2prob_odds(
        population,
        pop_samples,
        mtov,
        samples,
        mass,
        priors,
        spin=None,
        rtov=None,
        pop_weights=None,
        weights=None,
        verbose=False,
    ):
    """computes Monte Carlo expectation values and variances needed to compute probability and odds ratios
    """

    ### check input

    # check whether we will include spin info
    assert (spin is not None) == (rtov is not None), 'must specify both spin and rmax or neither of them'
    include_spin = spin is not None

    # check pop_weights
    Npop = 1.*len(mtov)
    if pop_weights is None:
        pop_weights = np.ones(Npop, dtype=float)
    else:
        assert len(pop_weights)==Npop, 'pop_weights must be the same length as mtov'

    # check single-event weights
    Nmass = 1.*len(mass)
    if weights is None:
        weights = np.ones(Nmass, dtype=float)
    else:
        assert len(weights)==Nmass, 'weights must be the same length as mass'

    #---

    ### we compute several expectation values and variance estimates for Monte-Carlo integrals with weighed samples
    ### we do not require weights to be normalized, and instead we normalize them ourselves

    # F = double-sum with conditional
    # G = double-sum without conditional (equivalent to a single sum over population)

    # probability : P = F/G
    # odds ratio  : O = F/(G-F)

    # compute moments
    (F, F_var), (G, G_var), FG_cov = _compute_pop_moments(
        population,
        pop_samples,
        mtov,
        pop_weights,
        samples,
        mass,
        priors,
        weights,
        spin=spin,
        rtov=rtov,
        verbose=verbose,
    )

    # assemple these moments into probability and odds ratio
    prob, prob_stdv = _moments2prob(F, G, F_var, G_var, FG_cov)
    odds, odds_stdv = _moments2odds(F, G, F_var, G_var, FG_cov)

    # return
    return (prob, prob_stdv), (odds, odds_stdv)

#------------------------

def samples2prob_odds_bayes(
        population,
        pop_samples,
        mtov,
        post_samples,
        post_mass,
        post_priors,
        prior_samples,
        prior_mass,
        prior_priors,
        post_spin=None,
        prior_spin=None,
        rtov=None,
        pop_weights=None,
        post_weights=None,
        prior_weights=None,
        verbose=False,
    ):
    """computes Monte Carlo expectation values and variances needed to compute prior/posterior probability and \
odds ratios along with bayes factor
    """

    ### sanity-check input arguments
    assert (post_spin is not None) == (prior_spin is not None), \
        'must specify spin_post, spin_prior, and rmax or none of them'

    assert (post_post is not None) == (rtov is not None), \
        'must specify both spin_post, spin_prior, and rmax or none of them'

    Npop = 1.*len(mtov)
    if pop_weights is None:
        pop_weights = np.ones(Npop, dtype=float)
    else:
        assert len(pop_weights)==Npop, 'mmax_weights must be the same length as mmax'

    Npost = 1.*len(post_mass)
    if post_weights is None:
        post_weights = np.ones(Npost, dtype=float)
    else:
        assert len(post_weights)==Npost, 'mass_post_weights must be the same length as mass'

    Nprior = 1.*len(prior_mass)
    if prior_weights is None:
        prior_weights = np.ones(Nprior, dtype=float)
    else:
        assert len(prior_weights)==Nprior, 'mass_prior_weights must be the same length as mass'

    #---

    ### we compute several expectation values and variance estimates for Monte-Carlo integrals with weighed samples
    ### we do not require weights to be normalized, and instead we normalize them ourselves

    # F = double-sum over pop and post with conditional
    # H = double-sum over pop and prior with conditional
    # G = double-sum over pop and post without conditional (equivalent to a single sum over pop)

    # posterior probability : P = F/G
    # posterior odds ratio  : O = F/(G-F)

    # prior probability : P = H/G
    # prior odds ratio  : O = H/(G-H)

    # bayes factor : posterior odds / prior odds

    ### compute mass moments for each population sample
    if verbose:
        print('computing moments over population and single-event parameter uncertainty')

    F, H, G, FG_cov, HG_cov, FH_cov = _compute_all_pop_moments(
        population,
        pop_samples,
        mtov,
        pop_weights,
        post_samples,
        post_mass,
        post_priors,
        post_weights,
        prior_samples,
        prior_mass,
        prior_priors,
        prior_weights,
        post_spin=prior_spin,
        prior_spin=prior_spin,
        rtov=rtov,
        verbose=verbose,
    )

    F, F_var = F
    H, H_var = H
    G, G_var = G

    ### assemple these moments into probability and odds ratio

    # posterior
    post_prob, post_prob_stdv = _moments2prob(F, G, F_var, G_var, FG_cov)
    post_odds, post_odds_stdv = _moments2odds(F, G, F_var, G_var, FG_cov)

    # prior
    prior_prob, prior_prob_stdv = _moments2prob(H, G, H_var, G_var, HG_cov)
    prior_odds, prior_odds_stdv = _moments2odds(H, G, H_var, G_var, HG_cov)

    # bayes factor
    bayes, bayes_stdv = _moments2bayes(F, G, H, F_var, G_var, H_var, FG_cov, Fh_cov, HG_cov)

    # return
    return (post_prob, post_prob_stdv), \
        (post_odds, post_odds_stdv), \
        (prior_prob, prior_prob_stdv), \
        (prior_odds, prior_odds_stdv), \
        (bayes, bayes_stdv)
