"""a module that standardizes I/O for the library
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import numpy as np

### non-standard libraries
try:
    from gwdistributions import parse as gwdist_parse
except ImportError:
    gwdist_parse = None

from . import utils

#-------------------------------------------------

def load_samples(path, verbose=False, max_num_samples=None):
    """load samples from a CSV
    """
    if verbose:
        print('loading samples from : '+path)
    samples = np.genfromtxt(path, names=True, delimiter=',')

    if not samples.shape: # samples is 1D
        samples = samples.reshape((1))

    return _downsample(samples, verbose=verbose, max_num_samples=max_num_samples)

#------------------------

def _extract_weights(samples, weight_column=None, weight_column_is_log=False, verbose=False):
    if weight_column is not None:
        if verbose:
            print('extracting nontrival weights from column='+weight_column)
        weights = samples[weight_column]
        if weight_column_is_log:
            if verbose:
                print('    exponentiating weights')
            weights = np.exp(weights)
    else:
        weights = np.ones(len(samples), dtype=float)

    # return
    return weights

#------------------------

def _downsample(samples, max_num_samples, verbose=False):

    if max_num_samples is None:
        max_num_samples = np.infty

    if len(samples) > max_num_samples:
        if verbose:
            print('downsampling from %d -> %d samples'%(len(samples), max_num_samples))
        np.random.shuffle(samples) ### careful! modifies this in place
        samples = samples[:max_num_samples]

    elif verbose:
        print('current number of samples (%d) is less than or equal to the maximum requested (%.0f)' \
            % (len(samples), max_num_samples))

    return samples

#-------------------------------------------------

def load_population(config, verbose=True):
    """parse the population model from an INI file and check that it is defined over the necessary variates
    """
    if gwdist_parse is None:
        raise ImportError('could not import gwdistributions.parse')
    gen = gwdist_parse.parse(config, verbose=verbose)
    return gen, gen._params, gen.variates

def check_samples(gen, samples):
    """check that all required values are present in samples
    """
    for variate in gen.variates + gen.required:
        assert variate in samples.dtype.names, \
            'variate required by population model (%s) not present in samples!' % variate

#------------------------

def load_population_samples(
        path,
        weight_column=None,
        weight_column_is_log=False,
        max_num_samples=None,
        mtov_column_range=None,
        verbose=False,
        retain=None, # if specified, retain only these columns
    ):
    """load population posterior samples from a CSV, including EoS information
    """
    pop_samples = load_samples(path, verbose=verbose) # read on all samples

    # prune if requested
    if mtov_column_range is not None:
        col, x_range = mtov_column_range
        if verbose:
            print('pruning %s samples to fit within [%.6f, %.6f]' % (col, x_range[0], x_range[1]))
        truth = utils.prune(pop_samples[col], x_range=x_range)

        if verbose:
            print('    retained %d / %d samples' % (np.sum(truth), len(truth)))
        pop_samples = pop_samples[truth]

    # downsample
    pop_samples = _downsample(pop_samples, max_num_samples, verbose=verbose)

    # figure out weights
    pop_weights = _extract_weights(
        pop_samples,
        weight_column=weight_column,
        weight_column_is_log=weight_column_is_log,
        verbose=verbose,
    )

    # keep only a subset of columns
    if retain is not None:
        pop_samples = _downselect(pop_samples, retain, verbose=verbose)

    # return
    return pop_samples, pop_weights

#-------------------------------------------------

def load_event_samples(
        path,
        prior_column,
        prior_column_is_log=False,
        weight_column=None,
        weight_column_is_log=False,
        ranges=None, # used for pruning
        max_num_samples=None,
        verbose=True,
        retain=None,
    ):
    """load samples for single-event posteriors from a CSV
    """
    samples = load_samples(path, verbose=verbose) # load everything

    # prune
    if ranges is not None:
        truth = np.ones(len(samples), dtype=bool)
        for name, foo, x_range in ranges:
            if verbose:
                print('pruning %s samples to fit within [%.6f, %.6f]' % (name, x_range[0], x_range[1]))
            truth *= utils.prune(foo(samples), x_range=x_range)

        if verbose:
            print('    retained %d / %d samples' % (np.sum(truth), len(truth)))
        samples = samples[truth]

    # downsample
    samples = _downsample(samples, max_num_samples, verbose=verbose)

    # figure out weights
    weights = _extract_weights(
        samples,
        weight_column=weight_column,
        weight_column_is_log=weight_column_is_log,
        verbose=verbose,
    )

    # figure out priors
    priors = _extract_weights(
        samples,
        weight_column=prior_column,
        weight_column_is_log=prior_column_is_log,
        verbose=verbose,
    )

    # keep only a subset of columns
    if retain is not None:
        samples = _downselect(samples, retain, verbose=verbose)

    # return
    return samples, priors, weights

#-------------------------------------------------

def _downselect(samples, retain, verbose=False):
    """downselect the columns in samples
    """
    retain = set([col for col in retain if col is not None]) # keep at most one copy of each column
    assert len(retain), 'must retain at least one column'

    # make sure that all requested columns are present
    dtype = []
    for col in retain:
        assert col in samples.dtype.names, 'requested column (%s) not present in samples (%s)!' \
            % (col, ', '.join(samples.dtype.names))
        dtype.append((col, samples.dtype.fields[col][0]))

    # now downselect the data
    subset = np.empty(len(samples), dtype=dtype)
    for col, _ in dtype:
        subset[col] = samples[col]

    # return
    return subset
