"""dimensionful units
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

CGS_G = 6.674e-8        # newton's constant in (g^-1 cm^3 s^-2)
CGS_Msun = 1.989e33     # mass of the sun in (g)
CGS_c = (299792458*100) # speed of light in (cm/s)
