"""basic utilities and default values
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import numpy as np

#-------------------------------------------------

DEFAULT_PRIOR_COLUMN = 'prior'

#------------------------

DEFAULT_M1_COLUMN = 'mass_1_source'
DEFAULT_M2_COLUMN = 'mass_2_source'

DEFAULT_MASS_COLUMN = DEFAULT_M2_COLUMN
DEFAULT_SPIN_COLUMN = 'a_2'

DEFAULT_MTOV_COLUMN = 'Mtov'
DEFAULT_RTOV_COLUMN = 'Rtov'
DEFAULT_D_COLUMN = 'luminosity_distance'

#------------------------

DEFAULT_MTOV_RANGE = (0.1, 5.0) ### specified in Msun

DEFAULT_Q_RANGE = (0., 0.15)
DEFAULT_M_RANGE = (0.5, 35.0) ### specified in Msun
DEFAULT_MC_RANGE = (5.141979, 9.519249) ### specified in Msun
DEFAULT_D_RANGE = (0, 500) ### specified in Mpc

#------------------------

DEFAULT_NUM_SAMPLES = 100000

#-------------------------------------------------

def bounds(samples, weights, quantile=0.01):
    """determine appropriate bounds for a histogram
    """
    order = np.argsort(samples)
    cweights = np.cumsum(weights[order])
    cweights /= cweights[-1]

    m = np.interp(quantile, cweights, samples[order])
    M = np.interp(1-quantile, cweights, samples[order])

    return (m, M), (np.min(samples[weights>0]), np.max(samples[weights>0]))

#----------------------------

def prune(x, x_range):
    """removes any samples that are not in the prior ranges
    """
    x_min, x_max = x_range
    return (x_min <= x)*(x < x_max)

#-------------------------------------------------

def report_samples2stats(tup):
    print('    num=%d\n    mean=%.6e\n    median=%.6e\n    std=%.6e\n    min=%.6e\n    max=%.6e'%tup)

#-----------

def samples2stats(x, weights=None):
    """compute basic statistics based on weighted samples
    """
    if weights is None:
        weights = np.ones(len(x), dtype=float)
    weights = weights[:]/np.sum(weights) ### make a copy so we don't alter these in-place

    # compute easy statistics
    num = len(x)
    mean = np.sum(weights*x)
    stdv = (np.sum(weights*x**2) - mean**2)**0.5
    minx = np.min(x)
    maxx = np.max(x)

    # compute median
    order = np.argsort(x)
    cweights = np.cumsum(weights[order])
    median = np.interp(0.5, cweights, x[order])

    return (num, mean, median, stdv, minx, maxx)

#-------------------------------------------------

PROB_ODDS_TEMPLATE = '''\
    Probability: P(%s) = %.6f +/- %.9f
    Odds Ratio : O^{%s)_{else} = %.6f +/- %.9f'''

def report_prob_odds(*args, include_spin=False):
    label = _selection_label(include_spin)

    exp_post, std_post = args[0]
    exp_post_odds, std_post_odds = args[1]

    print(PROB_ODDS_TEMPLATE % (label, exp_post, std_post, label, exp_post_odds, std_post_odds))

#------------------------

POST_PRIOR_ODDS_TEMPLATE = '''\
    Prior Probability: P(%s) = %.6f +/- %.9f
    Prior Odds Ratio : O^{%s)_{else} = %.6f +/- %.9f
    Bayes Factor: B^{%s)_{else} = %.6f +/- %.9f'''

def report_prior_post_odds(*args, include_spin=False):
    label = _selection_label(include_spin)

    exp_prior, std_prior = args[0]
    exp_prior_odds, std_prior_odds = args[1]
    exp_bayes, std_bayes = args[4]

    report_prob_odds(args[2], args[3])
    print(POST_PRIOR_ODDS_TEMPLATE % (
        label,
        exp_prior,
        std_prior,
        label,
        exp_prior_odds,
        std_prior_odds,
        label,
        exp_bayes,
        std_bayes,
    ))

#------------------------

def _selection_label(include_spin):
    label = "mass <= max_mass(spin, eos)"
    if include_spin:
        label = label + 'AND spin <= max_spin(eos)'
    return label
